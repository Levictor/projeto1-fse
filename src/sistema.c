#include "../inc/sistema.h"
int chave = 1;

void rodar_sistema(){
  int res = -100;
  while(1){
    update_refs();
    inserir_informacoes(dados_temp.temp_ref, dados_temp.temp_amb, dados_temp.temp_int, chave);

    write_lcd();
    
    res = chave ? 
      (int) pid_controle(dados_temp.temp_int):
      on_off_controle(dados_temp.temp_ref, dados_temp.temp_int, res);


    write_csv(res);
    enviar_dados_uart(res);

    if(res > 0){
      aciona_gpio(RESI_ADDR, res);
      usleep(DELAY_GPIO);
      aciona_gpio(VENT_ADDR, 0);
      usleep(DELAY_GPIO);
    }
    else if(res <= 0 && res > -40){
      aciona_gpio(RESI_ADDR, 0);
      usleep(DELAY_GPIO);
      aciona_gpio(VENT_ADDR, 0);
      usleep(DELAY_GPIO);
    }
    else{
      aciona_gpio(VENT_ADDR, -1 * res);
      usleep(DELAY_GPIO);
      aciona_gpio(RESI_ADDR, 0);
      usleep(DELAY_GPIO);
    }
    sleep(1);
  }
}

void write_lcd(){
  char * str = malloc(20*sizeof(char));

  sprintf(str, "TI:%.2f TR:%.2f", dados_temp.temp_int, (float) dados_temp.temp_ref);
  lcdLoc(LINE1);
  typeln(str);
  
  sprintf(str, "TA:%.2f", dados_temp.temp_amb);
  lcdLoc(LINE2);
  typeln(str);

  free(str);
}

void write_csv(int res){
  FILE * fp = fopen("data.csv", "a");
  time_t rawtime;
  struct tm * timeinfo;
  char tempo[30];
  char pid[3] = {'P', 'I', 'D'};
  char on_off[6] = {'O', 'N', '-', 'O', 'F', 'F'};

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  memcpy(tempo, asctime(timeinfo), 30);
  for(int i = 0; tempo[i] != '\0'; i++)
    if(tempo[i] == '\n')
      tempo[i] = '\0';

  fprintf(fp, "%s;%d;%d;%.2f;%.2f;%s\n", tempo, res, dados_temp.temp_ref, dados_temp.temp_int, dados_temp.temp_amb, chave ? pid : on_off);

  fclose(fp);
}

void inicia_sistema(){
  if(wiringPiSetup() == -1)
    exit(1);

  fd = wiringPiI2CSetup(BME280_ADD);
  
  init_bme();
  lcd_init();
  inicializar_gpio();
  ClrLcd();

  dados_temp.sinal_controle_MAX = 100;
  dados_temp.sinal_controle_MIN = -100;


  pid_altera_constantes(5.0, 1.0, 5.0);
	altera_histerese(8);
  erro_total = 0;
  erro_anterior = 0;
  T = 1;
}

void update_refs(){
  if(opcoes[ID_CONTROLE].valor == 0.0){
    dados_temp.temp_ref = (int) solicitar_dados_uart(TEMP_POT, dados_temp.temp_ref);
    chave = (int) solicitar_dados_uart(EST_CHA, chave);
  }
  else{
    dados_temp.temp_ref = (int) opcoes[ID_TR].valor;
    chave = (int) opcoes[ID_ON_OFF].valor;
  }
  dados_temp.temp_int = solicitar_dados_uart(TEMP_INT, dados_temp.temp_int);
  pid_atualiza_ref(dados_temp.temp_ref);
}

void finalizar_sistema(){
  finaliza_gpio();
  endwin();
  exit(0);
}
