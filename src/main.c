#include "../inc/sistema.h"

int main(){
  signal(SIGINT, finalizar_sistema);

  FILE * fp = fopen("data.csv", "w");

  fprintf(fp, "TIME;RES;TEMP_REF;TEMP_INT;TEMP_AMB;SWITCH\n");

  fclose(fp);
  
  inicia_sistema();
  
  inicia_menu();

  rodar_sistema();

  finalizar_sistema();

  return 0;
}

