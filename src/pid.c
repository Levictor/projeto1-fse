#include"../inc/pid.h"
#include <ncurses.h>

void pid_altera_constantes(double Kp_, double Ki_, double Kd_){
  if(Kp_)
    Kp = Kp_;

  if(Ki_)
    Ki = Ki_;

  if(Kd_)
    Kd = Kd_;
}

void pid_atualiza_ref(double ref_){
  temp_referencia = ref_;
}

double pid_controle(double saida_medida){
  double erro = temp_referencia - saida_medida;

  erro_total += erro;

  if(erro_total >= dados_temp.sinal_controle_MAX){
    erro_total = dados_temp.sinal_controle_MAX;
  }
  else if(erro_total <= dados_temp.sinal_controle_MIN){
    erro_total = dados_temp.sinal_controle_MIN;
  }

  double delta_erro = erro - erro_anterior;

  sinal_controle = Kp*erro + (Ki*T)*erro_total + (Kd/T)*delta_erro;
  if(sinal_controle >= dados_temp.sinal_controle_MAX){
    sinal_controle = dados_temp.sinal_controle_MAX;
  }
  else if(sinal_controle <= dados_temp.sinal_controle_MIN){
    sinal_controle = dados_temp.sinal_controle_MIN;
  }

  erro_anterior = erro;

  return sinal_controle;
}
