#include"../inc/i2c.h"
#include"../inc/bme280.h"

void init_bme(){
  int8_t rslt = BME280_OK;
  struct bme280_dev dev;
  struct identifier id;
  
  id.dev_addr = BME280_I2C_ADDR_PRIM;
  dev.intf = BME280_I2C_INTF;
  dev.read = user_i2c_read;
  dev.write = user_i2c_write;
  dev.delay_us = user_delay_us;

  dev.intf_ptr = &id;
  char i2c_[] = "/dev/i2c-1";

  if((id.fd = open(i2c_, O_RDWR)) < 0){
    fprintf(stderr, "Failed to open the i2c bus %X\n", BME280_ADD);
    return init_bme();
  }



  if(ioctl(id.fd, I2C_SLAVE, id.dev_addr) < 0){
    fprintf(stderr, "Failed to acquire bus access and/or talk to slave.\n");
  }


  rslt = bme280_init(&dev);
  if(rslt != BME280_OK){
    fprintf(stderr, "Failed to stream sensor data (code %+d).\n", rslt);
  }

  rslt = stream_sensor_data_normal_mode(&dev);

  close(id.fd);

}

int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr){
  struct identifier id = *((struct identifier*) intf_ptr);

  write(id.fd, &reg_addr, 1);
  read(id.fd, data, len);

  return 0;
}

void user_delay_us(uint32_t period, void *intf_ptr){
  usleep(period);
}

void print_sensor_data(struct bme280_data *comp_data){
    float temp;

#ifdef BME280_FLOAT_ENABLE
    temp = comp_data->temperature;
#else
#ifdef BME280_64BIT_ENABLE
    temp = 0.01f * comp_data->temperature;
#else
    temp = 0.01f * comp_data->temperature;
#endif
#endif
    dados_temp.temp_amb = temp;
}

int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr)
{
    uint8_t *buf;
    struct identifier id;

    id = *((struct identifier *)intf_ptr);

    buf = malloc(len + 1);
    buf[0] = reg_addr;
    memcpy(buf + 1, data, len);
    if (write(id.fd, buf, len + 1) < (uint16_t)len)
    {
        return BME280_E_COMM_FAIL;
    }

    free(buf);

    return BME280_OK;
}

int8_t stream_sensor_data_normal_mode(struct bme280_dev *dev){
  int8_t rslt;
  uint8_t settings_sel;
  struct bme280_data comp_data;

  /* Recommended mode of operation: Indoor navigation */
  dev->settings.osr_h = BME280_OVERSAMPLING_1X;
  dev->settings.osr_p = BME280_OVERSAMPLING_16X;
  dev->settings.osr_t = BME280_OVERSAMPLING_2X;
  dev->settings.filter = BME280_FILTER_COEFF_16;
  dev->settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

  settings_sel = BME280_OSR_PRESS_SEL;
  settings_sel |= BME280_OSR_TEMP_SEL;
  settings_sel |= BME280_OSR_HUM_SEL;
  settings_sel |= BME280_STANDBY_SEL;
  settings_sel |= BME280_FILTER_SEL;
  rslt = bme280_set_sensor_settings(settings_sel, dev);
  rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, dev);

  dev->delay_us(100000, dev->intf_ptr);
  rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
  print_sensor_data(&comp_data);

  return rslt;
}


void lcd_init(){
  fd_LCD = wiringPiI2CSetup(DSPLCD_ADD);

  // Initialise display
  lcd_byte(0x33, LCD_CMD); // Initialise
  lcd_byte(0x32, LCD_CMD); // Initialise
  lcd_byte(0x06, LCD_CMD); // Cursor move direction
  lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
  lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
  lcd_byte(0x01, LCD_CMD); // Clear display
  delayMicroseconds(500);
}

void lcd_toggle_enable(int bits){
  // Toggle enable pin on LCD display
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd_LCD, (bits | ENABLE));
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd_LCD, (bits & ~ENABLE));
  delayMicroseconds(500);
}

void typeln(const char *s){
  while(*s) lcd_byte(*(s++), LCD_CHR);
}

void lcdLoc(int line){
  lcd_byte(line, LCD_CMD);
}

void ClrLcd(void){
  lcd_byte(0x01, LCD_CMD);
  lcd_byte(0x02, LCD_CMD);
}

void lcd_byte(int bits, int mode)   {

  //Send byte to data pins
  // bits = the data
  // mode = 1 for data, 0 for command
  int bits_high;
  int bits_low;
  // uses the two half byte writes to LCD
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT ;
  bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT ;

  // High bits
  wiringPiI2CReadReg8(fd_LCD, bits_high);
  lcd_toggle_enable(bits_high);

  // Low bits
  wiringPiI2CReadReg8(fd_LCD, bits_low);
  lcd_toggle_enable(bits_low);
}

