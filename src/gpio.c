#include"../inc/gpio.h"

void inicializar_gpio(){
  pinMode(RESI_ADDR, PWM_OUTPUT);
  pinMode(VENT_ADDR, PWM_OUTPUT);
  softPwmCreate(RESI_ADDR, 1, 100);
  softPwmCreate(VENT_ADDR, 1, 100);
}

void aciona_gpio(int PWM_pin, int intensidade){
  softPwmWrite(PWM_pin, intensidade);
  usleep(DELAY_GPIO);
}

void finalizar_gpio(){
  softPwmWrite(RESI_ADDR, 0);
  usleep(DELAY_GPIO);
  softPwmWrite(VENT_ADDR, 0);
  usleep(DELAY_GPIO);
}
