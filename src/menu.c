#include "../inc/menu.h"

void inicia_menu(){
  iniciox = 0;
  inicioy = 0;
  opcoes[ID_CONTROLE].chave = "CONTROLE";
  opcoes[ID_CONTROLE].valor = 0.0;
  opcoes[ID_ON_OFF].chave = "ON-OFF";
  opcoes[ID_ON_OFF].valor = 0.0;
  opcoes[ID_TR].chave = "TR";
  opcoes[ID_TR].valor = 9.0;
  opcoes[ID_HISTERESE].chave = "HISTERESE";
  opcoes[ID_HISTERESE].valor = 5.0;
  opcoes[ID_KP].chave = "KP";
  opcoes[ID_KP].valor = 5.0;
  opcoes[ID_KI].chave = "KI";
  opcoes[ID_KI].valor = 1.0;
  opcoes[ID_KD].chave = "KD";
  opcoes[ID_KD].valor = 5.0;
  opcoes[ID_SAIR].chave = "SAIR";
  opcoes[ID_SAIR].valor = 0.0;
  pthread_t menu;

  initscr();
  clear();
  noecho();
  cbreak();       /* Buffer de linha desativado. passa tudo */

  iniciox = (80 - WIDTH) / 2;
  inicioy = (24 - HEIGHT) / 2;

  cabecalho_win = newwin(HEIGHT - 5, WIDTH, 2, iniciox);

  menu_win = newwin(HEIGHT, WIDTH, inicioy + 3, iniciox);

  keypad(menu_win, TRUE);
  mvprintw(0, 0, "Use as setas para subir e descer, pressione Enter para selecionar uma escolha");
  refresh();
  print_header(cabecalho_win);
  refresh();
  print_menu(menu_win, 0);
  pthread_create(&menu, NULL, menu_interativo, NULL);
}

void *menu_interativo(){
  int escolha = -1;
  int destaque = 0;
  int c;
	int menu_disponivel = 1;
  while( menu_disponivel ){
    c = wgetch(menu_win);
    switch(c){
      case 107:
      case KEY_UP:
        if(destaque == ID_CONTROLE)
          destaque = ID_SAIR;
        else
          --destaque;
        print_menu(menu_win, destaque);
        break;

      case 106:
      case KEY_DOWN:
        if(destaque == ID_SAIR)
          destaque = 0;
        else
          ++destaque;
        print_menu(menu_win, destaque);
        break;

      case 10:
        escolha = destaque;
        break;

      default:
        mvprintw(24, 0, "                                                                       ");
        refresh();
        mvprintw(24, 0, "O número do caractere que você digitou é = %3d e corresponde à: '%c'", c, c);
        refresh();
        break;
    }

    float valor;

    if(escolha == ID_CONTROLE){
      opcoes[ID_CONTROLE].valor = !opcoes[ID_CONTROLE].valor;
      print_menu(menu_win, escolha);
      escolha = -1;
    }
    else if(escolha == ID_ON_OFF){
      opcoes[ID_ON_OFF].valor = !opcoes[ID_ON_OFF].valor;
      print_menu(menu_win, destaque);
      escolha = -1;
    }
    else if( escolha > 0 && escolha != ID_SAIR && opcoes[ID_CONTROLE].valor){
      mvprintw(23, 0, "                            ");
      refresh();
      echo();
      mvscanw(23,0,"%f\n", &valor);
      noecho();
      refresh();
      switch(escolha){
        case ID_HISTERESE:
          // HISTERESE
          opcoes[ID_HISTERESE].valor = valor;
          altera_histerese((int) valor);
          break;
        case ID_TR:
          // TR
          opcoes[ID_TR].valor = valor;
          pid_atualiza_ref(valor);
          break;
        case ID_KP:
          // KP
          opcoes[ID_KP].valor = valor;
          pid_altera_constantes((double) valor, 0.0, 0.0);
          break;
        case ID_KI:
          // KI
          opcoes[ID_KI].valor = valor;
          pid_altera_constantes(0.0, (double) valor, 0.0);
          break;
        case ID_KD:
          // KD
          opcoes[ID_KD].valor = valor;
          pid_altera_constantes(0.0, 0.0, (double) valor);
          break;
        case ID_SAIR:
          menu_disponivel = 0;
          break;
      }
      print_menu(menu_win, escolha);
      escolha = -1;
    }

    else if(escolha == ID_SAIR)
      menu_disponivel = 0;
  }

  clrtoeol();
  refresh();
	finalizar_sistema();
}

void inserir_informacoes(int temp_ref_n, float temp_ext_n, float temp_int_n, int chave){
  temp_ref_men = temp_ref_n;
  temp_ext_men = temp_ext_n;
  temp_int_men = temp_int_n;
  print_header(cabecalho_win);
}
void print_header(WINDOW *cabecalho_win){
  int x, y;

  x = 2;
  y = 2;
  box(cabecalho_win, 0, 0);
  mvwprintw(cabecalho_win, y, x, "Temperatura Interna: %.02f", temp_int_men);
  y++;
  mvwprintw(cabecalho_win, y, x, "Temperatura Externa: %.02f", temp_ext_men);
  y++;
  mvwprintw(cabecalho_win, y, x, "Temperatura Referencia: %.02f", temp_ref_men);
  wrefresh(cabecalho_win);
}

void print_menu(WINDOW *menu_win, int destaque){
  int x, y, i;

  x = 2;
  y = 2;
  box(menu_win, 0, 0);
  for(i = 0; i < N_OPCOES; i++){
    if(destaque == i)
      wattron(menu_win, A_REVERSE);
    if(i == ID_CONTROLE){
      mvwprintw(menu_win, y, x, "                            ");
      mvwprintw(menu_win, y, x, "%s - %s", &opcoes[i].chave[0], opcoes[i].valor ? "TERMINAL" : "POTENCIOMETRO");
    }
    else if(i == ID_ON_OFF){
      mvwprintw(menu_win, y, x, "                            ");
      mvwprintw(menu_win, y, x, "%s - %s", &opcoes[i].chave[0], opcoes[i].valor ? "PID" : "ON-OFF");
    }
    else if(i == ID_SAIR)
      mvwprintw(menu_win, y, x, "%s", &opcoes[i].chave[0]);
    else
      mvwprintw(menu_win, y, x, "%s - %.2f", &opcoes[i].chave[0], opcoes[i].valor);
    if(destaque == i)
      wattroff(menu_win, A_REVERSE);
    
    ++y;
  }
  wrefresh(menu_win);
}

