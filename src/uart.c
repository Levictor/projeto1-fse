#include "../inc/uart.h"

unsigned char MATRICULA[4] = {5, 2, 0, 8};

void inicializa_uart(int * uart0_filestream){
  
  //Open in non blocking read/write mode
  *uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
  
  if(*uart0_filestream == -1)
    printf("Erro - Não foi possível iniciar a UART.\n");

  struct termios options;
  inicializa_opcoes(&options, *uart0_filestream);

}

void inicializa_opcoes(struct termios * options, int uart0_filestream){

  tcgetattr(uart0_filestream, options);
  options->c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
  options->c_iflag = IGNPAR;
  options->c_oflag = 0;
  options->c_lflag = 0;
  tcflush(uart0_filestream, TCIFLUSH);
  tcsetattr(uart0_filestream, TCSANOW, options);

}

void prepara_pacote(unsigned char *pacote, char endereco_dispositivo, char codigo_solicitacao, char comando){
  memcpy(&pacote[0], &endereco_dispositivo, 1);
  memcpy(&pacote[1], &codigo_solicitacao, 1);
  memcpy(&pacote[2], &comando, 1);
  memcpy(&pacote[3], MATRICULA, 4);
}

void enviar_dados_uart(int valor){
  int uart0_filestream = -1;
  inicializa_uart(&uart0_filestream);
  if(uart0_filestream == -1)exit(1);

  unsigned char pacote[20];
  prepara_pacote(pacote, 0x01, 0x16, 0xD1);

  memcpy(&pacote[7], &valor, 4);
  uint16_t crc = gen_crc16(pacote, 11);
  memcpy(&pacote[11], &crc, 2);

  int count = write(uart0_filestream, &pacote[0], 13);
  if(count < 0)
    printf("UART TX error\n");
}

float solicitar_dados_uart(unsigned char sub_cod, float temp_ant){

  int uart0_filestream = -1;

  inicializa_uart(&uart0_filestream);
  if(uart0_filestream == -1)exit(1);

  unsigned char pacote[10];

  prepara_pacote(pacote, 0x01, 0x23, sub_cod);

  uint16_t crc = gen_crc16(pacote, 7);
  memcpy(&pacote[7], &crc, 2);

  int count = write(uart0_filestream, &pacote[0], 9);
  if(count < 0)
    return temp_ant;

  sleep(1);

  float valor_sensor;
  int valor_chave;
  
  if(uart0_filestream != -1){
    unsigned char rx_buffer[12];

    int rx_length = read(uart0_filestream, rx_buffer, 10);
    if(rx_length < 0)
      return temp_ant;
    else if (rx_length == 0)
      return temp_ant;
    else if(rx_length == 12){
      if(sub_cod == EST_CHA){
        memcpy(&valor_chave, &rx_buffer[6], 4);
        return (float) valor_chave;
      }
      memcpy(&valor_sensor, &rx_buffer[6], 4);
      return valor_sensor;
    }
    else{
      if(sub_cod == EST_CHA){
        memcpy(&valor_chave, &rx_buffer[6], 4);
        return (float) valor_chave;
      }
      memcpy(&valor_sensor, &rx_buffer[3], 4);
      return valor_sensor;
    }
  }
  return temp_ant;
}
