#include "../inc/on_off.h"

int on_off_controle(int temp_ref, int temp_int, int sinal){
  if(temp_int >= temp_ref + (histerese / 2))
    return -100;
  else if(temp_int <= temp_ref - (histerese / 2))
    return 100;
  else
    return sinal;
}

void altera_histerese(int n_histerese){
  histerese = n_histerese;
}
