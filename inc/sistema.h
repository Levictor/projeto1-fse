#ifndef SISTEMA_H
#define SISTEMA_H

#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>

//#include<bcm2835.h>
#include "./on_off.h"
#include "./crc.h"
#include "./uart.h"
#include "./pid.h"
#include "./lcd_driver.h"
#include "./i2c.h"
#include "./data.h"
#include "./gpio.h"
#include "./menu.h"

void rodar_sistema();
void write_lcd();
void write_csv(int);
void inicia_sistema();
void update_refs();
void finalizar_sistema();

#endif

