#ifndef UART_H
#define UART_H

#include "../inc/crc.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART

#define TEMP_INT 0xC1
#define TEMP_POT 0xC2
#define EST_CHA 0xC3


void inicializa_uart(int *);
void inicializa_opcoes(struct termios*, int);
void enviar_dados_uart(int);
float solicitar_dados_uart(unsigned char, float);
void prepara_pacote(unsigned char *, char, char, char);


#endif
