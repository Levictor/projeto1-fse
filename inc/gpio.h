#ifndef GPIO_H
#define GPIO_H

#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define RESI_ADDR 4
#define VENT_ADDR 5

#define DELAY_GPIO 10000

void inicializar_gpio();
void aciona_gpio(int,int);
void finalizar_gpio();


#endif
