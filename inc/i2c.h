#ifndef I2C_H
#define I2C_H

#include <string.h>
#include<linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include"./bme280.h"
#include"./bme280_defs.h"
#include"./data.h"
#include"./lcd_driver.h"

//struct bme280_dev dev;
struct identifier{
  /* Variable to hold device address */
  uint8_t dev_addr;

  /* Variable that contains file descriptor */
  int8_t fd;
};
struct identifier id;

int fd;
dados_ext dados_temp;

// Endereços de comunicação I2C
#define BME280_ADD 0x76 // Sensor BM280
#define DSPLCD_ADD 0x27 // Display LCD

// Constantes
#define MAX_LEN 20

int fd_LCD, fd_BME;

//const int sinal_controle_MAX = 100, sinal_controle_MIN = -100;


// functions
void init_bme();

// LCD functions
// Definidas no driver_lcd

// Sensor BM280
int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void*intf_ptr);
void user_delay_us(uint32_t period, void *intf_ptr);
void print_sensor_data(struct bme280_data *comp_data);
int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr);
int8_t stream_sensor_data_normal_mode(struct bme280_dev *dev);

#endif

