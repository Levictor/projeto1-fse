#ifndef MENU_H
#define MENU_H

#include <ncurses.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "./gpio.h"
#include "./pid.h"
#include "./on_off.h"

#define WIDTH 33
#define HEIGHT 12
#define N_OPCOES 8
#define ID_CONTROLE 0
#define ID_ON_OFF 1
#define ID_TR 2
#define ID_HISTERESE 3
#define ID_KP 4
#define ID_KI 5
#define ID_KD 6
#define ID_SAIR 7

typedef struct menu_struct{
  char *chave;
  float valor;
}menu_struct;

menu_struct opcoes[N_OPCOES];

float temp_ref_men, temp_ext_men, temp_int_men;

int iniciox;
int inicioy;

WINDOW *menu_win;
WINDOW *cabecalho_win;

void print_menu(WINDOW *menu_win, int destaque);
void inicia_menu();
void print_header(WINDOW *);
void *menu_interativo();
void inserir_informacoes(int, float, float, int);

#endif
