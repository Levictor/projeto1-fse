# Projeto1 - FSE

Projeto1 - FSE

## Introdução

Esse projeto é uma solução em C para o controle de um sistema contendo sensores, um resistor, uma ventoinha e um display LED, tudo isso fazendo uso de uma RaspberryPi.

## Como utilizar

Para fazer uso do programa basta realizar a instalação das dependências, rodar o comando ```make``` para compilar o programa e gerar o arquivo binário, após isso é necessário apenas rodar o comando ```make run```.

Após a inicialização do programa você verá um menu como esse: 

![menu](./assets/Screenshot\ from\ 2021-09-06\ 18-18-50.png)

Dentro dele você poderá escolher a opção de controlar a temperatura e constantes pelo terminal ou pelo potenciometro selecionando a opção CONTROLE, após isso todas as opções do menu serão alteráveis, antes disso nenhuma opção é selecionável, exceto pela opção SAIR.

Após alterar o controle para o estado TERMINAL, você poderá selecionar as temperaturas e as constantes de acordo com o que desejar, quando selecionar algum valor basta digitar o valor desejado e apertar enter, o mesmo pode ser visto abaixo do menu.

**Cuidado ao selecionar as temperaturas**

## Dados coletados

O sistema faz uso de dois métodos de controle de temperatura, sendo um o PID e outro o On-Off. O programa gera hm arquivo CSV a partir da sua execução, e com base nele foram montados os seguintes gráficos:

### PID

1. Temperatura

![Temp_PID](./assets/PID-TEMP.png)

2. Sinal

![sinal_PID](./assets/PID-SINAL.png)

### ON-OFF

1. Temperatura

![Temp_ON_OFF](./assets/ON-OFF-TEMP.png)

2. Sinal

![Sinal_ON_OFF](./assets/ON-OFF-SINAL.png)
